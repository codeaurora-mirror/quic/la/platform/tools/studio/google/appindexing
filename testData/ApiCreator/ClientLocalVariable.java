package com.example.creation;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends Activity {
  <caret>
  @Override
  public void onStart() {
    super.onStart();
    GoogleApiClient client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    client.connect();
    Action viewAction = Action.newAction(
        Action.TYPE_VIEW,
        "Main Page",
        Uri.parse("http://www.example.com/main"),
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    AppIndex.AppIndexApi.start(client, viewAction);
  }
}
