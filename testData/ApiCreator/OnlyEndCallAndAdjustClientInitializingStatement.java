package com.example.creation;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends Activity {
  <caret>
  private GoogleApiClient client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    client = new GoogleApiClient.Builder(this).build();
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
    Action viewAction = Action.newAction(
        Action.TYPE_VIEW,
        "Main Page",
        Uri.parse("http://www.example.com/main"),
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    AppIndex.AppIndexApi.end(client, viewAction);
  }
}
