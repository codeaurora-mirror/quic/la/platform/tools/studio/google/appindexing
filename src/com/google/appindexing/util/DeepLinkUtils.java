/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appindexing.util;

import com.android.SdkConstants;
import com.google.common.collect.Lists;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Util functions for searching deep links.
 * <p/>
 * For more information, see
 * http://developer.android.com/training/app-indexing/deep-linking.html
 */
public final class DeepLinkUtils {
  private static final String TAG_ACTION = "action";
  private static final String TAG_CATEGORY = "category";

  private static final String ACTION_VIEW = "android.intent.action.VIEW";
  private static final String CATEGORY_DEFAULT = "android.intent.category.DEFAULT";
  private static final String CATEGORY_BROWSABLE = "android.intent.category.BROWSABLE";

  /**
   * Returns all matching deep links from a root xml tag.
   *
   * @param root The root xml tag, usually is the root tag of AndroidManifest.xml
   * @return All matching deep links
   */
  @NotNull
  public static List<String> getAllDeepLinks(@NotNull XmlTag root) {
    List<XmlTag> intentFilters = ManifestUtils.searchXmlTagsByName(root, SdkConstants.TAG_INTENT_FILTER);
    List<String> deepLinks = Lists.newArrayList();
    for (XmlTag intentFilter : intentFilters) {
      String deepLink = getDeepLinkFromIntentFilter(intentFilter);
      if (deepLink != null) {
        deepLinks.add(deepLink);
      }
    }
    return deepLinks;
  }

  /**
   * Returns the deep link within an intent filter xml tag.
   */
  @Nullable
  private static String getDeepLinkFromIntentFilter(@NotNull XmlTag intentFilter) {
    // Check action.
    List<XmlTag> actions = ManifestUtils.searchXmlTagsByName(intentFilter, TAG_ACTION);
    boolean hasActionView = false;
    for (XmlTag action : actions) {
      String name = action.getAttributeValue(SdkConstants.ATTR_NAME, SdkConstants.NS_RESOURCES);
      if (name != null && name.equals(ACTION_VIEW)) {
        hasActionView = true;
        break;
      }
    }
    if (!hasActionView) {
      return null;
    }

    // Check category
    List<XmlTag> categories = ManifestUtils.searchXmlTagsByName(intentFilter, TAG_CATEGORY);
    boolean hasDefaultCategory = false;
    boolean hasBrowsableCategory = false;
    for (XmlTag category : categories) {
      String name = category.getAttributeValue(SdkConstants.ATTR_NAME, SdkConstants.NS_RESOURCES);
      if (name != null && name.equals(CATEGORY_DEFAULT)) {
        hasDefaultCategory = true;
      }
      else if (name != null && name.equals(CATEGORY_BROWSABLE)) {
        hasBrowsableCategory = true;
      }
    }
    if (!hasDefaultCategory || !hasBrowsableCategory) {
      return null;
    }

    // Parse deep link
    List<XmlTag> datas = ManifestUtils.searchXmlTagsByName(intentFilter, SdkConstants.TAG_DATA);
    String scheme = null, host = null, pathPrefix = null, path = null;
    for (XmlTag data : datas) {
      if (scheme == null) {
        scheme = data.getAttributeValue(SdkConstants.ATTR_SCHEME, SdkConstants.NS_RESOURCES);
      }
      if (host == null) {
        host = data.getAttributeValue(SdkConstants.ATTR_HOST, SdkConstants.NS_RESOURCES);
      }
      if (pathPrefix == null) {
        pathPrefix = data.getAttributeValue(SdkConstants.ATTR_PATH_PREFIX, SdkConstants.NS_RESOURCES);
      }
      if (path == null) {
        path = data.getAttributeValue(SdkConstants.ATTR_PATH, SdkConstants.NS_RESOURCES);
      }
    }

    // Build a deep link, which should look something like
    // "http://", "http://host", "http://host/path-prefix" or "http://host/path.html".
    // Incomplete deep links are OK since we only want to generate what we can,
    // and AppIndexingApiDetector will issue errors for users to fix bad deep links.
    if (scheme != null) {
      StringBuilder buf = new StringBuilder(scheme);
      buf.append("://");
      if (host != null) {
        buf.append(host);
        if (path != null && path.startsWith("/")) {
          buf.append(path);
        }
        else if (pathPrefix != null && pathPrefix.startsWith("/")) {
          buf.append(pathPrefix);
        }
      }
      return buf.toString();
    }
    return null;
  }
}

